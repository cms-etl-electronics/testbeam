# Testbeam

This repository is meant as code base for test beam data taking.
Original code in this repo is minimal, it relies on submodules that handle the Lecroy scope readout and ETROC DAQ readout.

To clone this repository:

``` shell
git clone --recursive ssh://git@gitlab.cern.ch:7999/cms-etl-electronics/testbeam.git
```

## Structure

### module_test_sw / tamalero

Code to configure FE electronics and take data with KCU105 based DAQ.

### ScopeHandler

TBA

## Usage

TBA

